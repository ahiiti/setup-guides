Setup Guides by `ahiiti`
========================

This repository contains documentation to set up some software that I use at my work or privately.

It takes a lot of time to figure out all the relevant tweaks and best-practices, so hopefully this can be useful for you.


Available documentations:
- Nextcloud server on Debian 10

Coming soon:
- Arch linux featuring the Sway Window Manager
- MediaWiki server
- Automated local or remote backup solution
- Reverse proxy using `systemd` and `autossh`