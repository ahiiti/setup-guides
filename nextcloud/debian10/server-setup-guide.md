This document is in development and has not been tested yet!
============================================================



Preparation
===========

- Investigate the network environment: Does your Server have a public domain, static or dynamic IP?
- It is recommended to have a domain name.
	+ Some free DNS providers:
		* https://freedns.afraid.org (huge collection of subdomains)
		* https://dyn.com, https://www.noip.com (Dynamic DNS, supported by most home routers)
		* https://my.freenom.com/domains.php (Free **top-level** domains! .tk .ml .ga .cf)
  	+ Some suggested paid providers:
	  	* https://inwx.de
	  	* https://namecheap.com

Creating the installation medium
================================

First, download the debian installation image for your processor architecture (amd64 most certainly on modern machines) from [https://www.debian.org/distrib/netinst]().

On Linux
--------

Plug in the flash drive and identify the device name, for example using `lsblk`

    $ lsblk -o NAME,MODEL,SIZE
    NAME   MODEL                      SIZE
    loop0                             2.9G
    sda    WDC_WD10EURX-63FH1Y0     931.5G
    ├─sda1                            499M
    ├─sda2                            100M
    ├─sda3                             16M
    └─sda4                          930.9G
    sdb    DataTraveler_3.0          14.4G
    ├─sdb1                            3.1G
    └─sdb2                            736K
    sr0    HL-DT-ST_DVDRAM_GH24NSD1 160.5M

In this example, the device name is `sdb`, so the path to our flash drive is `/dev/sdb`. Replace `/dev/sdb` in this example with your device path.

Before flashing, we have to unmount all partitions of this drive:

    $ umount /dev/sdb*

Now, write the image to the flash drive:

    $ sudo dd if=Downloads/debian-10.0.0-amd64-netinst.iso of=/dev/sdb status=progress bs=1M oflag=sync

The newly written partitions may also get mounted automatically, so unmount again:

    $ umount /dev/sdb*

Unplug the flash drive.


On Windows
-------

Use a tool like the [LinuxLive USB Creator](https://linuxliveusb.com/) or [Rufus](https://rufus.ie/).


Installing Debian 10
====================

Insert the installation flash drive.
Boot from the debian installation medium.

Choose `Install` if you are comfortable with using only the keyboard or `Graphical install` if you want to use the mouse. Both are essentially the same.

- Choose your locale settings and keyboard layout.

Network configuration
---------------------

- enter the host name.
- enter the domain name

For example, if this server is reachable at `cloud.mydomain.com`, it would be `cloud` and `mydomain.com`. You can also make something up if you don't have a domain yet or only use this at home.

### Network authentication fails

This means that the network is not configured to automatically assign IP addresses.
- Select `Configure network manually`
- Enter the IP address
- Enter the netmask
- Enter the gateway
- Enter the DNS server address

Set up users and passwords
--------------------------

- Choose a password for the `root` user.

- Enter a name, username and password for your user account.

Partition disks
---------------
Use these instructions if you aren't experienced. This will probably fit for most scenarios.

- Choose `Guided - use entired disk`
- Select the disk you want to install the operating system on.
- Choose `All files in one partition`.
- Choose `Finish partitioning and write changes to disk` when you are done.

Configure the package manager
-----------------------------

- Choose `No` when you are asked if you want to scan another CD or DVD.

- Choose the debian archive mirror country.

Normally, you should choose the same country as your server's location.

- Choose the mirror.

If you have a high-speed link to one of the mirror's network, you might get even faster speeds. Otherwise, the default one is OK.

- Leave the HTTP proxy information blank.

Configuring popularity-contest
------------------------------

- Select wether you want the developers to get software usage statistics from your server.

Software selection
------------------

Check the boxes as follows:

- [ ] Debian desktop environment
- [ ] ... GNOME
- [ ] ... Xfce
- [ ] ... KDE Plasma
- [ ] ... Cinnamon
- [ ] ... MATE
- [ ] ... LXDE
- [ ] ... LXQt
- [ ] web server
- [ ] print server
- [x] SSH server
- [x] standard system utilities


Install the GRUB boot loader on a hard disk
-------------------------------------------

- Choose yes.

- Choose the same disk that you chose for installation earlier.

The installation is now complete. Follow the instructions on the screen to reboot into debian.

Command line access
===================

The following chapters have to be done inside a server shell.

You could just use the keyboard and monitor connected to your server to set up everything, though it is much more comfortable to remotely access it via SSH.

On Linux
--------
Just use the `ssh` command.

    $ ssh username@server.domain

On Windows
----------
Microsoft included SSH in Windows 10 by default as late as April 2018.
If you have an older Windows version, use a tool like [Putty](https://putty.org) to connect via ssh.

Otherwise, open PowerShell and use the `ssh` command.

    PS C:\Users\user> ssh username@server.domain


Root privileges
===============

Almost everything we have to do requires root privileges, so log in as the root user.

    $ su -l

Diffie-Hellman parameter
========================

We need this later for encryption. Execute this in a seperate window, it will take some time.

    openssl dhparam -out /etc/ssl/dhparam.pem 4096

Packages to install
===================

Basic system utilities
----------------------
These are some utilities that are commonly used.

    apt install zip unzip lm-sensors htop net-tools screen

nginx
-----
The web server.

    apt install nginx

PHP
---
Support for the PHP language.

    apt install php php-fpm

PHP modules
-----------
Source: https://docs.nextcloud.com/server/latest/admin_manual/installation/source_installation.html#prerequisites-for-manual-installation

PHP modules that are required or recommended for nextcloud.

    apt install php-{common,mysql,zip,xml,mbstring,curl,gd,json,bz2,intl,imagick,apcu,bcmath,gmp}

Thumbnail image generation
------------------------
Required for preview image generation.

    apt install imagemagick ffmpeg libreoffice

MariaDB
-------
The database server application.

    apt install mariadb-server

certbot
-------
Manages https certificates provided by Let's Encrypt.

    apt install certbot python-certbot-nginx

ZFS
---
Source: https://github.com/zfsonlinux/zfs/wiki/Debian, https://wiki.debian.org/ZFS

Add the backports repository:

	echo -e 'deb http://deb.debian.org/debian buster-backports main contrib\ndeb-src http://deb.debian.org/debian buster-backports main contrib' > /etc/apt/sources.list.d/buster-backports.list


Install ZFS:

    apt update
    apt install linux-headers-amd64
    apt install -t buster-backports dkms spl-dkms
    apt install -t buster-backports zfs-dkms zfsutils-linux

If the installation fails, run

    /sbin/modprobe zfs

and try again.
This is probably caused by a bug in the installation script.

ZFS setup
=========

(TODO: Identify drives by UUID and not by path!)

Identify the data drive's names:

    lsblk

Create a mirrored pool named `data` that will be mounted in `/data` (Replace `/dev/sdb` and `/dev/sdc` with your drive letters.)

    zpool create data mirror /dev/sdb /dev/sdc -m /data

Check everything with `lsblk`, `zfs list`, `zpool list` and `zpool status`.


Create a ZFS dataset `/data/nextcloud` with full rights to the user `www-data`.

When nextcloud has it's own ZFS dataset, it makes handling data independently from other datasets easier (e.g. snapshots or file system options).

    cd /data
    zfs create data/nextcloud
    chown www-data:www-data nextcloud
    chmod 770 nextcloud

This will be the data directory for nextcloud.

Create a dataset `data/tmp` and the directory `/data/tmp/php`, which will be used for temporary files during large uploads by PHP.

    zfs create data/tmp
    cd /data/tmp
    mkdir php
    chown -R www-data:www-data php

PHP-FPM configuration
=====================
Source: https://docs.nextcloud.com/server/latest/admin_manual/installation/source_installation.html#php-ini-configuration-notes

Source: https://docs.nextcloud.com/server/latest/admin_manual/installation/source_installation.html#php-fpm-configuration-notes

Source: https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/big_file_upload_configuration.html?highlight=php%20ini#configuring-php

Source: https://docs.nextcloud.com/server/latest/admin_manual/installation/server_tuning.html#enable-php-opcache


Open `/etc/php/7.3/fpm/pool.d/www.conf`

 - uncomment the line `clear_env = no`

The following changes have to be made in two files:
 - `/etc/php/7.3/fpm/php.ini` (used by the web server)
 - `/etc/php/7.3/cli/php.ini` (used by the command line interface and CRON jobs)

Uncomment and/or modify the following lines:

- `opcache.enable=1`
- `opcache.enable_cli=1`
- `opcache.memory_consumption=128`
- `opcache.interned_strings_buffer=8`
- `opcache.max_accelerated_files=10000`
- `opcache.revalidate_freq=1`
- `opcache.save_comments=1`
- `max_input_time = 3600`
- `max_execution_time = 3600`
- `upload_tmp_dir = /data/tmp/php`
- `upload_max_filesize = 64G`
- `post_max_size = 64G`
- `output_buffering = 0`
- `memory_limit = 512M`

You may adjust the maximum upload size and maximum execution time to your own needs.

Modify `/etc/php/7.3/fpm/pool.d/www.conf` according to the [Nextcloud Administration Manual](https://docs.nextcloud.com/server/latest/admin_manual/installation/server_tuning.html#tune-php-fpm):

 - `pm = dynamic`
 - `pm.max_children = 120`
 - `pm.start_servers = 12`
 - `pm.min_spare_servers = 6`
 - `pm.max_spare_servers = 18`


Restart PHP

    systemctl restart php7.3-fpm


MariaDB configuration
=====================

Source: https://docs.nextcloud.com/server/latest/admin_manual/configuration_database/linux_database_configuration.html

Run the initial setup of MariaDB:

    mysql_secure_installation

- Don't set a root password (you authenticate by being root in linux)

- Remove the anonymus user

- Disallow root login remotely

- Remove test database

- Reload privilege table

Start the MySql CLI as root:

    mariadb -u root

Create a nextcloud user and database with the utf8mb4 charset:

    CREATE USER 'nextcloud'@'localhost' IDENTIFIED BY 'password';
    CREATE DATABASE nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
    GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'localhost';
    FLUSH privileges;
    quit;

Backup `/etc/mysql/my.cnf`:

    cp /etc/mysql/my.cnf /etc/mysql/my.cnf.backup

Change `/etc/mysql/my.cnf` according to the documentation: [Nextcloud Administration
Manual *Database configuration*](https://docs.nextcloud.com/server/latest/admin_manual/configuration_database/linux_database_configuration.html)

Adjust caching:
Source: https://docs.nextcloud.com/server/latest/admin_manual/installation/server_tuning.html#using-mariadb-mysql-instead-of-sqlite

At the section `[mysqld]`, add/edit the following:

 - `innodb_buffer_pool_size=1G`
 - `innodb_io_capacity=4000`

Restart MariaDB:

    systemctl restart mariadb

HTTPS Certificate
=================

Run certbot to obtain a certificate:

    certbot certonly --nginx --rsa-key-size 4096

All keys are now stored in `/etc/letsencrypt/live/domain.com/` .

Certbot has also automatically created a cronjob at `/etc/cron.d/certbot`, which will attempt to renew the certificate regularly.

Installing nextcloud
====================

Download the latest version of nextcloud to `/var/www`:

    cd /var/www
    wget https://download.nextcloud.com/server/releases/latest.zip
    unzip latest.zip
    rm latest.zip

Set the owner of nextcloud to `www-data`:

    chown -R www-data:www-data nextcloud

nginx configuration
-------------------

Source: https://docs.nextcloud.com/server/latest/admin_manual/installation/nginx.html

Remove the default nginx site:

    rm /etc/nginx/sites-available/*
    rm /etc/nginx/sites-enabled/*

Create a file `/etc/nginx/sites-available/nextcloud`. Paste and modify
the default nextcloud config from the Nextcloud Admin Manual *Nginx
configuration*:

-	comment `server 127.0.0.1:9000`, uncomment `server unix:/var/run/phpx.x-fpm.sock` and adjust `x.x` to the installed PHP version (for Debian 10: `server unix:/var/run/php/php7.3-fpm.sock;`)

-   adjust all lines `server_name cloud.example.com;` to your domain

-   lines starting with `root` should be `root /var/www/nextcloud/;`
    
- Use Mozilla's guidelines for SSL/TLS settings: https://ssl-config.mozilla.org/
(check installed versions using `nginx -v` and `openssl version`) **If you don't know better, choose intermediate!**

Change the paths for keys and certificates:
    ssl_certificate /etc/letsencrypt/live/cloud.example.com/fullchain.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/cloud.example.com/chain.pem;
    ssl_certificate_key /etc/letsencrypt/live/cloud.example.com/privkey.pem;

Uncomment/change the path of the Diffie-Hellman parameter which we generated earlier:
    ssl_dhparam /etc/ssl/dhparam.pem;

Enable the site by creating a symlink in the `sites-enabled`
directory.

    ln -s /etc/nginx/sites-available/nextcloud /etc/nginx/sites-enabled/nextcloud

Restart nginx.

    systemctl restart nginx


Web interface setup
-------------------

Open the address of your nextcloud in a browser. You should see the initial configuration interface of nextcloud.

-   Data directory: `/data/nextcloud`

-   Database name and user: `nextcloud`

-   Database url: `localhost:3306`

Optimziation
------------

### Memory Cache, file locking

Source: https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/caching_configuration.html#id2

Install Redis:

    apt install redis-server php-redis

Check if Redis is running:

    ps ax | grep redis

Activate Memory Cache: Open `/var/www/nextcloud/config/config.php` and
add the line

	'memcache.local' => '\OC\Memcache\APCu',
	'filelocking.enabled' => 'true'
	'memcache.locking' => '\OC\Memcache\Redis',
	'redis' => [
	     'host'     => '/var/run/redis/redis-server.sock',
	     'port'     => 0,
	     'timeout'  => 1.5,
	],

Edit `/etc/redis/redis.conf`:

- Uncomment the line `unixsocket /var/run/redis/redis-server.sock`
- change the line `port 6379` to `port 0`
- change and uncomment the line `unixsocketperm 700` to `unixsocketperm 770`

Grant the required rights:

     usermod -a -G redis www-data

Restart the redis server:

    systemctl restart redis-server

### Cronjob

Add a cronjob that will run nextclods cron.php every 5 minutes as the
user `www-data`.

Open crontab:

    sudo -u www-data crontab -e

Append the following line:

    */5 * * * * php -f /var/www/nextcloud/cron.php

Nextcloud will configure itself accordingly when the cronjob executes the first time.

Security
--------

If you want to disable password resets, add this line to
`/var/www/nextcloud/config/config.php`

    'lost_password_link' => 'disabled',

Further configurations
======================
Have a look at https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/config_sample_php_parameters.html to see further configuration options.

Default language and locale
---------------------------
Source: https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/language_configuration.html

Change the default language and locale by inserting the following settings into `/var/www/nextcloud/config/config.php` (This example is personal german):

    'default_language' => 'de',
    'default_locale' => 'de_DE',

Default files for new users
-----------------
Source: https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/config_sample_php_parameters.html#user-experience

By default, the content of `/var/www/nextcloud/core/skeleton` are copied to new user's directories.

If you want to change the default files, use a different directory instead.

    'skeletondirectory' => '/data/default_files`,

Make sure that the user `www-data` has read access to this folder.

Leave it empty if you don't want default files:

    'skeletondirectory' => '',



Post-installation
=================

Check your server against vulnerabilities:

-   <https://scan.nextcloud.com> to check for nextcloud-specific
    vulnerabilities

-   <https://ssllabs.com/ssltest> to check for ssl security

Import old files to new users
=============================

To copy all data from folder `/path/to/old/data/` to `/data/nextcloud/username/files/`, run

    rsync --recursive --progress /path/to/old/data/* /data/nextcloud/username/files/
    chown -R www-data:www-data /data/nextcloud/username/files
    sudo -u www-data php /var/www/nextcloud/occ files:scan --path="/username/files" --verbose

Parameters `--progress` or `--verbose` are optional.


Uninterruptible power supply
============================

For APC UPSs and other compatible UPS models, install `apcupsd`

    apt install apcupsd

This may also work with other UPS's with a compatible protocol.
In the output of `cat /var/log/messages`, you should see a line like:

    <date> <time> <hostname> kernel: [...] hid-generic ... [UPS Model Name] on usb [...]

You can also filter for buzzwords, like

    cat /var/log/messages | grep -i usb
    cat /var/log/messages | grep -i ups

With `lsusb`, the UPS should also show up.
Open `/etc/apcupsd/apcupsd.conf`:

|   Option   |                Value                   |
| ---------- | -------------------------------------- |
| `UPSNAME`  | The name of the UPS in system messages |
| `UPSCABLE` | Probably `usb`                         |
| `UPSTYPE`  | Probably `usb`                         |
| `DEVICE`   | remove or comment out the line (`#`)   |


Other probably interesting options:

    ONBATTERYDELAY BATTERYLEVEL MINUTES TIMEOUT ANNOY ANNOYDELAY

After `systemctl restart apcupsd`, it should work. Check with
`apcaccess`.

Check the functionality of system messages and automatic shutdown with
another electricity consumer, like a bulb or a dummy PC first, before
plugging the server into the UPS!
You can use `watch apcaccess` to monitor the UPS's status in real time.
For further optimization, diagnosis and self-testing, use `apctest`.
Logs are stored in `/var/log/apcupsd.events`.

Common troubleshooting issues
=============================

ZFS
---

### The data pool is not imported after power on

Useful commands

    systemctl status zfs-import-cache.service
    zdb

If the output of `zdb | grep path` looks like `path: '/dev/sdx'`, zfs
can't import the pool because it can't find it by the drive's ID's.\
Export the pool (if imported) and re-import it using `/dev/disk/by-id`:

    zpool export data
    zpool import -d /dev/disk/by-id data
